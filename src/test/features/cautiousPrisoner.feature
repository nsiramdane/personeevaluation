Feature: Cautious prisoner

  Scenario: A cautious prisoner one betrays until you cooperate, then he will cooperate
    Given a cautious prisoner, Dave
    And a vengeful prisoner, Tim
    When Dave and Tim play together
    Then Dave will start by betraying
    But Dave will cooperate in the following round